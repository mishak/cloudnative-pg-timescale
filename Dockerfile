ARG BASE
FROM $BASE

USER root
ARG TIMESCALE_VERSION

RUN apt-get update \
    && apt-get install -y lsb-release wget \
    && echo "deb https://packagecloud.io/timescale/timescaledb/debian/ $(lsb_release -c -s) main" | tee /etc/apt/sources.list.d/timescaledb.list \
    && wget --quiet -O - https://packagecloud.io/timescale/timescaledb/gpgkey | apt-key add - \
    && apt-get update \
    && apt-get install -y "timescaledb-2-postgresql-${PG_MAJOR}=${TIMESCALE_VERSION}~*" "timescaledb-2-loader-postgresql-${PG_MAJOR}=${TIMESCALE_VERSION}~*" \
    && apt-get remove -y lsb-release wget \
    && rm -fr /tmp/* \
    && rm -rf /var/lib/apt/lists/*

USER 26
